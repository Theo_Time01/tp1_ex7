#  TP1 Exercice 7

## Objectif 
L'objectif de cet exercice est de faire des logging des différents fonctions
des packages utilisés.

## Realisation 
J'ai créé deux packages, un permettant de stocker le script de SimpleComplexCalculator et un autre package contenant un script de test.

## Organisation
voici l'arborescence de notre projet :
```
.
├── Package_Calculator
│   ├── Calculator.py
│   ├── __init__.py
│   └── __pycache__
│       
├── Package_Test
│   ├── exo7.py
│   └── __init__.py
│ 
└── README.md
```


## Lancement
il faut d'abord entrer la commande :  
  
    export PYTHONPATH=$PYTHONPATH:'.'

puis on execute la commande :  

    python3 ./Package_Test/exo7.py

## Résultat
```
multiplication de deux tuples => OK
.multiplication de deux tuples(entier positif, entier négatif)=>OK
.multiplication tuples avec un entier et un caractère =>Erreur=>OK
.soustraction tuples avec un entier et un caractère =>Erreur=>OK
.soustraction de deux tuples => OK
.soustraction de deux tuples(entier positif, entier négatif)=>OK
.somme de deux tuples(entier positif, entier négatif)=>OK
.somme de deux tuples => OK
.somme tuples avec un entier et un caractère =>Erreur=>OK
.division de deux tuples => OK
.division de deux tuples(entier positif, entier négatif)=>OK
.division tuples avec un entier et un caractère =>Erreur=>OK
.Test division par 0 => OK
.
----------------------------------------------------------------------
Ran 13 tests in 0.001s

OK
```


